<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8" import="chrisxue815.quiz.models.Question"%>
<%
  Question question = (Question) request.getAttribute("question");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Question</title>
</head>
<body>
  <p>
    Q<%=question.getId()%>:
    <%=question.getContent()%>
  </p>
  <form action="index" method="post">
    <input type="hidden" name="questionId" value="<%=question.getId()%>" />
    <p>
      <label><input type="radio" name="answer" value="1"
        checked="checked" />(a) YES</label>
    </p>
    <p>
      <label><input type="radio" name="answer" value="0" />(b)
        NO</label>
    </p>
    <p>
      <input type="submit" value="Submit"
        onclick="this.disabled=true; this.form.submit();" /> <input
        type="reset" value="Reset" />
    </p>
  </form>
</body>
</html>
