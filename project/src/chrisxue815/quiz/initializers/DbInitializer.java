package chrisxue815.quiz.initializers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Application Lifecycle Listener implementation class Init
 * 
 */
@WebListener
public class DbInitializer implements ServletContextListener {

  /**
   * Default constructor.
   */
  public DbInitializer() {
  }

  /**
   * @see ServletContextListener#contextInitialized(ServletContextEvent)
   */
  public void contextInitialized(ServletContextEvent arg0) {
    Connection con = null;
    try {
      // load the jdbc driver
      Class.forName("org.sqlite.JDBC");

      // create a database connection and a statement
      con = DriverManager.getConnection("jdbc:sqlite:db.sqlite");
      Statement stmt = con.createStatement();

      // create a table containing usernames and their scores
      stmt.addBatch("create table if not exists users(username text primary key, score integer)");
      
      // create a table containing questions and answers
      stmt.addBatch("create table if not exists questions(id integer primary key, content text, answer boolean)");
      
      // create a table containing user's answers to questions
      stmt.addBatch("create table if not exists users_questions(username text not null, question_id integer not null, answer boolean, primary key(username, question_id))");

      // insert questions and answers
      stmt.addBatch("insert or ignore into questions(id, content, answer) values(1, 'Do Dogs bark?', 1)");
      stmt.addBatch("insert or ignore into questions(id, content, answer) values(2, 'Do Rivers flow upstream?', 0)");
      stmt.addBatch("insert or ignore into questions(id, content, answer) values(3, 'Are Deserts lush?', 0)");
      stmt.addBatch("insert or ignore into questions(id, content, answer) values(4, 'Are Slaves free?', 0)");
      stmt.addBatch("insert or ignore into questions(id, content, answer) values(5, 'Are Oceans filled with water?', 1)");
      stmt.addBatch("insert or ignore into questions(id, content, answer) values(6, 'Do Liars speak the truth?', 0)");
      stmt.addBatch("insert or ignore into questions(id, content, answer) values(7, 'Is Fire hot?', 1)");
      stmt.addBatch("insert or ignore into questions(id, content, answer) values(8, 'Is Vinegar sour?', 1)");
      stmt.addBatch("insert or ignore into questions(id, content, answer) values(9, 'Is Blood red?', 1)");
      stmt.addBatch("insert or ignore into questions(id, content, answer) values(10, 'Is Ice hot?', 0)");

      stmt.executeBatch();
    }
    catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    finally {
      try {
        if (con != null)
          con.close();
      }
      catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * @see ServletContextListener#contextDestroyed(ServletContextEvent)
   */
  public void contextDestroyed(ServletContextEvent arg0) {
  }

}
