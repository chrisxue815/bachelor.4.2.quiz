package chrisxue815.quiz.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import chrisxue815.quiz.models.Question;

/**
 * Database Access Object
 * 
 */
public class IndexDAO {

  public static int getPreviousScore(String username) {
    Connection con = null;
    Statement stmt = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    int score = -2;

    try {
      // create a database connection
      con = DriverManager.getConnection("jdbc:sqlite:db.sqlite");

      pstmt = con.prepareStatement("select score from users where username=?");
      pstmt.setString(1, username);
      rs = pstmt.executeQuery();

      if (rs.next()) {
        score = rs.getInt("score");
      }
      else {
        // if there's no score, it means the user is visiting this website the first time
        rs.close();
        pstmt.close();
        
        // get all questions the user has answered
        pstmt = con.prepareStatement("select * from users_questions where username=?");
        pstmt.setString(1, username);
        rs = pstmt.executeQuery();
        
        // -1 means the user has answered some questions
        // -2 means the user is in the welcome page
        score = rs.next() ? -1 : -2;
        
        rs.close();
        pstmt.close();
        
        // insert the score which also indicates the current state
        pstmt = con.prepareStatement("insert into users(username, score) values(?,?)");
        pstmt.setString(1, username);
        pstmt.setInt(2, score);
        pstmt.executeUpdate();
      }
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    finally {
      closeDb(con, stmt, pstmt, rs);
    }

    return score;
  }

  public static Question submitAnswer(String username, int questionIdFromRequest,
      boolean answer) {
    Connection con = null;
    Statement stmt = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    Question question = null;
    int questionId;

    try {
      // create a database connection
      con = DriverManager.getConnection("jdbc:sqlite:db.sqlite");

      // get the id of the previous answered question
      pstmt = con
          .prepareStatement("select max(question_id) from users_questions where username=?");
      pstmt.setString(1, username);
      rs = pstmt.executeQuery();

      if (rs.next()) {
        // if the user has answered some questions, go to the next question
        questionId = rs.getInt("max(question_id)") + 1;
      }
      else {
        // if it's the first question
        questionId = 1;
      }

      rs.close();
      pstmt.close();

      // make sure the user is submitting answer to the correct question
      if (questionId == questionIdFromRequest) {
        // insert the answer
        pstmt = con
            .prepareStatement("insert into users_questions(username, question_id, answer) values(?,?,?)");
        pstmt.setString(1, username);
        pstmt.setInt(2, questionId);
        pstmt.setBoolean(3, answer);
        pstmt.executeUpdate();

        pstmt.close();

        ++questionId;
      }

      // get the next question
      pstmt = con.prepareStatement("select content from questions where id=?");
      pstmt.setInt(1, questionId);
      rs = pstmt.executeQuery();

      if (rs.next()) {
        // if there are remaining questions
        String content = rs.getString("content");
        question = new Question(questionId, content);
      }
      else {
        // if the user has answered all questions
        rs.close();
        pstmt.close();

        // calculate the score, i.e., count the number of correct answers
        pstmt = con
            .prepareStatement("update users set score=(select count(*) from questions join users_questions on questions.id=question_id where username=? and questions.answer=users_questions.answer) where username=?");
        pstmt.setString(1, username);
        pstmt.setString(2, username);
        pstmt.executeUpdate();
      }
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    finally {
      closeDb(con, stmt, pstmt, rs);
    }

    return question;
  }

  public static Question getFirstQuestion(String username) {
    Connection con = null;
    Statement stmt = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    Question question = null;

    try {
      // create a database connection
      con = DriverManager.getConnection("jdbc:sqlite:db.sqlite");
      
      // delete previous answers
      pstmt = con
          .prepareStatement("delete from users_questions where username=?");
      pstmt.setString(1, username);
      pstmt.executeUpdate();

      pstmt.close();

      // set the current state to answering questions
      pstmt = con
          .prepareStatement("update users set score=-1 where username=?");
      pstmt.setString(1, username);
      pstmt.executeUpdate();

      // get the first question
      stmt = con.createStatement();
      rs = stmt.executeQuery("select content from questions where id=1");

      if (rs.next()) {
        String content = rs.getString("content");
        question = new Question(1, content);
      }
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    finally {
      closeDb(con, stmt, pstmt, rs);
    }

    return question;
  }

  public static Question getCurrentQuestion(String username) {
    Connection con = null;
    Statement stmt = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    Question question = null;

    try {
      // create a database connection
      con = DriverManager.getConnection("jdbc:sqlite:db.sqlite");

      // get the id of the previous answered question
      pstmt = con
          .prepareStatement("select max(question_id) from users_questions where username=?");
      pstmt.setString(1, username);
      rs = pstmt.executeQuery();
      int questionId;

      if (rs.next()) {
        // if the user has answered some questions, go to the next question
        questionId = rs.getInt("max(question_id)") + 1;
      }
      else {
        // if it's the first question
        questionId = 1;
      }

      rs.close();
      pstmt.close();

      // get the current question
      pstmt = con.prepareStatement("select content from questions where id=?");
      pstmt.setInt(1, questionId);
      rs = pstmt.executeQuery();

      if (rs.next()) {
        String content = rs.getString("content");
        question = new Question(questionId, content);
      }
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    finally {
      closeDb(con, stmt, pstmt, rs);
    }

    return question;
  }

  private static void closeDb(Connection con, Statement stmt,
      PreparedStatement pstmt, ResultSet rs) {
    try {
      if (rs != null)
        rs.close();
      if (stmt != null)
        stmt.close();
      if (pstmt != null)
        pstmt.close();
      if (con != null)
        con.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
  }

}
