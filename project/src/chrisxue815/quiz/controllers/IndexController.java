package chrisxue815.quiz.controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chrisxue815.quiz.db.IndexDAO;
import chrisxue815.quiz.models.Question;

/**
 * Servlet implementation class Index
 */
@WebServlet("/index")
public class IndexController extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public IndexController() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
   *      response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // get the username
    String username = getUsername(request, response);

    // get the previous score which also indicates the current state
    int score = IndexDAO.getPreviousScore(username);

    // url of the result page
    String resultPage;

    if (score == -2) {
      // if the user is visiting this website the first time, show the welcome page
      resultPage = "/WEB-INF/jsp/welcome.jsp";
    }
    else if (score == -1) {
      // if the user is answering question, show the question
      Question question = IndexDAO.getCurrentQuestion(username);

      if (question == null) {
        resultPage = "/WEB-INF/jsp/welcome.jsp";
      }
      else {
        resultPage = "/WEB-INF/jsp/question.jsp";
        request.setAttribute("question", question);
      }
    }
    else {
      // if the user has answered all questions, show the result page
      resultPage = "/WEB-INF/jsp/result.jsp";
      request.setAttribute("score", score);
    }

    // forward to a specific result page
    RequestDispatcher dispatcher = request.getRequestDispatcher(resultPage);
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
   *      response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // get the username
    String username = getUsername(request, response);
    
    // get the question id and the user's answer from request
    String questionIdStr = request.getParameter("questionId");
    String answerStr = request.getParameter("answer");
    
    // the next question
    Question nextQuestion;
    
    // url of the result page
    String resultPage;

    if (questionIdStr == null || answerStr == null) {
      // if the request lacks one of the parameters, show the user the first question
      nextQuestion = IndexDAO.getFirstQuestion(username);
    }
    else {
      // submit the answer
      int questionId = Integer.parseInt(questionIdStr);
      boolean answer = answerStr.equals("1") ? true : false;
      nextQuestion = IndexDAO.submitAnswer(username, questionId, answer);
    }

    if (nextQuestion == null) {
      // if the user has answered all questions, show the result page
      resultPage = "/WEB-INF/jsp/result.jsp";
      int score = IndexDAO.getPreviousScore(username);
      request.setAttribute("score", score);
    }
    else {
      // if there are remaining questions, show the question page
      resultPage = "/WEB-INF/jsp/question.jsp";
      request.setAttribute("question", nextQuestion);
    }

    // forward to a specific result page
    RequestDispatcher dispatcher = request.getRequestDispatcher(resultPage);
    dispatcher.forward(request, response);
  }

  private String getUsername(HttpServletRequest request,
      HttpServletResponse response) {
    
    // demonstrate cookies accessing
    String sessionId = request.getSession().getId();
    setCookie(response, "sessionId", sessionId);

    Cookie[] cookies = request.getCookies();
    String username = getCookie(cookies, "username");

    username = request.getRemoteUser();
    setCookie(response, "username", username);

    return username;
  }

  private String getCookie(Cookie[] cookies, String name) {
    if (cookies != null) {
      for (int i = 0; i < cookies.length; i++) {
        if (cookies[i].getName().equals(name)) {
          return cookies[i].getValue();
        }
      }
    }
    return null;
  }

  private void setCookie(HttpServletResponse response, String name, String value) {
    Cookie sessionIdCookie = new Cookie(name, value);
    sessionIdCookie.setMaxAge(-1);
    response.addCookie(sessionIdCookie);
  }

}
